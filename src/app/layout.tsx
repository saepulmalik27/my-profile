import "./globals.css";
import { Teko } from "next/font/google";
import Image from "next/image";
import Link from "next/link";
import { twMerge } from "tailwind-merge";

const inter = Teko({
  weight: ["300", "400", "500", "600", "700"],
  subsets: ["latin"],
});

export const metadata = {
  title: "Saepul Malik",
  description: "My personal website",
};

const navLinks = [
  {
    href: "#about",
    label: "About",
  },
  {
    href: "#experience",
    label: "Experience",
  },
  {
    href: "#work",
    label: "Work",
  },
  {
    href: "#contact",
    label: "Contact",
  },
];

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className="h-full">
      <body
        className={twMerge(inter.className, "grid grid-rows-sandwich h-full bg-black text-secondary")}
      >
        <header className="flex justify-between px-10 py-3 sticky top-0 left-0 w-full" >
          <Image src={"/assets/logo.png"} width={40} height={40} alt="logo" />
          <nav className="counter-nav">
            <ol className="list-none flex gap-5 h-full items-center">
              {navLinks.map(({ href, label }, key) => (
                <li key={key} className="text-xl"  >
                  <Link href={href} className="hover:text-primary">{label}</Link>
                </li>
              ))}
            </ol>
          </nav>
        </header>
        {children}
        <footer className="px-10 py-3" >&copy; Saepul Malik 2023</footer>
      </body>
    </html>
  );
}
