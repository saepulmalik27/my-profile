import About from '@/components/about/About'
import Hero from '@/components/hero/Hero'
import Work from '@/components/jobs/Jobs'
import Image from 'next/image'

export default function Home() {
  return (
    <main className=' counter-section px-10 py-3'>
     <Hero/>
     <About/>
     <Work/>
    </main>
  )
}
