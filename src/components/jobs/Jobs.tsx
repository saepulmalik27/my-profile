"use client";
import React, { useState } from "react";

const jobs = [
  {
    title: "Frontend Engineer",
    company: "PT Inspigo Inovasi Indonesia",
    id: "inspigo",
    date: "February 2021 - Present",
    description:
      "I am currently working at Inspigo as a Frontend Engineer. using ReactJS and NextJS as my main tools to build a website.",
    responsibilities: [
      "Developed, maintained, and shipped production code for client event websites primarily using ReactJS, NextJS, GatbsyJS",
      "Developed, maintained, and shipped production code for company  platform website (business.inspigo.id) primarily using ReactJS and NextJS",
    ],
  },
  {
    title: "Web Developer",
    company: "PT Praweda Sarana Informatika",
    id: "praweda",
    date: "September 2018 - February 2021",
    description:
      "I am currently working at Praweda Ciptakarsa Informatika as a Web Developer. using Laravel as my main tools to build a website.",
    responsibilities: [
      "Developed, maintained, and shipped production code for client websites primarily using Laravel",
    ],
  },
  {
    title: "IT Consultant",
    company: "PT Indocyber Global Teknologi",
    id: "iglo",
    date: "April 2018 - September 2018",
    description:
      "I am currently working at Iglo as a IT Consultant. using Laravel as my main tools to build a website.",
    responsibilities: [
      "Developed, maintained, and shipped production code for client websites primarily using Laravel",
    ],
  },
];

type TJob = {
  title: string;
  company: string;
  id: string;
  date: string;
  description: string;
  responsibilities: string[];
};

const Work = () => {
  const [selectedJob, setSelectedJob] = useState<TJob>(jobs[0]);

  const handleSelectedJob = (job: TJob) => {
    setSelectedJob(jobs[0]);
  };

  return (
    <section className="max-w-2xl my-0 mx-auto py-20" id="about">
      <h2 className="count-section text-subheading leading-relaxed text-white">
        Work Experience
      </h2>

      <div className="flex gap-10">
        <div className="flex flex-col">
          {jobs.map((item, key) => (
            <div
              key={key}
              className="px-5 h-12 flex items-center justify-start w-fit whitespace-nowrap  border-primary border-l-2  cursor-pointer  hover:border-white  hover:text-white transition duration"
              onClick={() => {
                handleSelectedJob(item);
              }}
            >
              <p className="text-xl">{item.company}</p>
            </div>
          ))}
        </div>

        <div>
          {selectedJob.responsibilities.map((item, key) => (
            <p key={key} className="text-xl">{item}</p>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Work;
