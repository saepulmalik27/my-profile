"use client";
import React from "react";
import { useInView } from "react-intersection-observer";
import { twMerge } from "tailwind-merge";

const Hero = () => {
  const { inView,  ref } = useInView({
    triggerOnce: true,
  });

  return (
    <section ref={ref}
      className={twMerge("max-w-5xl flex h-screen min-h-screen flex-col justify-center items-start my-0 mx-auto", inView ? "animate-fade-in-up" : "opacity-0")}
    >
      <h1 className="text-primary text-xl leading-8">Hello!, my name is</h1>
      <div>
        <h2 className="text-heading leading-none text-white">Saepul Malik.</h2>
      </div>
      <div>
        <h3 className="text-heading leading-none">I am a Web Developer.</h3>
      </div>
      <p className="text-xl max-w-lg">
        I am a web developer based in Indonesia, I have been developing websites
        for over 5 years. currently I am working at{" "}
        <a
          href="https://www.inspigo.id/"
          target="_blank"
          rel="noopener noreferrer"
          className="text-primary"
        >
          Inspigo
        </a>{" "}
        as a Frontend Engineer. using ReactJS and NextJS as my main tools to
        build a website.
      </p>
    </section>
  );
};

export default Hero;
