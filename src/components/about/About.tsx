
"use client";

import Image from "next/image";
import React from "react";
import { useInView } from "react-intersection-observer";
import { twMerge } from "tailwind-merge";

const toollist = [
  "React",
  "NextJs",
  "Vuejs",
  "GatsbyJs",
  "Javascript",
  "Node",
  "Graphql",
  "Jest",
  "Sass",
  "Styled-components",
  "css",
  "tailwindcss",
  "Laravel",
  "PHP",
  "Git",
];

const About = () => {

  const {ref, inView} = useInView({
    triggerOnce: true,
  })

  console.log(inView);
  

  return (
    <section className={twMerge("max-w-4xl my-0 mx-auto py-20",inView ?  "animate-fade-in-up" : "opacity-0")} id="about" ref={ref}>
      <h2 className="count-section text-subheading leading-relaxed text-white">About me</h2>

      <div className="grid grid-cols-3/2 gap-10">
        <article className="flex flex-col gap-5">
          <p className="text-xl leading-8">
            Hello! My name is Malik and I am a web developer. My interest in web
            development started back in 2011 when I decided to try editing
            custom Blogspot themes — it taught me a lot about HTML & CSS!
          </p>
          <p className="text-xl leading-8">
            Fast-forward to today, and I’ve had the privilege of working at an
            IT Solutions Company , a huge corporation and a start-up. My main
            focus now is developing a web platform that provides learning
            opportunities for professionals to advance their careers through the
            contents that we offered on our company (Inspigo.id) platform.
          </p>
          <p className="text-xl leading-8">
            I am still continuously learning about web development advancements
            and framework developments for helpme to stay up to date. Here are
            some tools that I frequently use:
          </p>
          <ul className="grid grid-cols-3">
          {toollist.map((item, key) => (
            <li className="toollist text-xl"  key={key}>{item}</li>
          ))}
        </ul>
        </article>
   

        <div>
          <Image
            src={"/assets/profile-4x.png"}
            width={500}
            height={500}
            alt="profile"
          />
        </div>
      </div>
    </section>
  );
};

export default About;
