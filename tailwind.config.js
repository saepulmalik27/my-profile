/** @type {import('tailwindcss').Config} */

const colors = require('./src/config/colors')

module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      gridTemplateRows: {
        "sandwich": "auto 1fr auto"
      },
      gridTemplateColumns: {
        "3/2" : "3fr 2fr",
      },
      fontSize : {
        "heading" : "clamp(40px, 8vw, 80px)",
        "subheading" : "clamp(26px, 5vw, 32px)",
      },
      colors: colors
    },
  },
  plugins: [],
}
